package com.metlife.capstone.mongodb;


import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import static java.util.concurrent.TimeUnit.SECONDS;


public class App 
{
    public static void main( String[] args )
    {
    	MongoClient mongoClient;
		try {
			
			mongoClient = new MongoClient( "localhost", 27017 );
			
			DB db = mongoClient.getDB( "test" );
			
			Set<String> colls = db.getCollectionNames();

			for (String s : colls) {
			    System.out.println(s);
			}
			
			DBCollection col = db.getCollection("col");
			
			BasicDBObject docToInsert = new BasicDBObject("name", "fromJava").append("status", "successful");
			
			col.insert(docToInsert);
			
			BasicDBObject docFromQuery = new BasicDBObject("name", "fromJava");
			
			DBCursor cursor = col.find(docFromQuery);
			
			try{
				while (cursor.hasNext()){
					System.out.println(cursor.next());
				}
			} finally {
				cursor.close();
			}
			
			
			mongoClient.close();
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.err.println("Problems doing mongo things");
			e.printStackTrace();
		}     	
    	
    }
}
